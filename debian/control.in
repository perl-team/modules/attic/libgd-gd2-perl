Source: libgd-perl
Section: perl
Priority: extra
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>,
 Dominic Hargreaves <dom@earth.li>,
 Xavier Guimard <x.guimard@free.fr>,
 Vasudev Kamath <kamathvasudev@gmail.com>
Build-Depends: @cdbs@
Vcs-Git: git://anonscm.debian.org/pkg-perl/packages/libgd-perl
Vcs-Browser: http://anonscm.debian.org/?p=pkg-perl/packages/libgd-perl.git
Standards-Version: 3.9.4
Homepage: http://search.cpan.org/dist/GD/

Package: libgd-perl
Architecture: any
Depends: ${shlibs:Depends}, ${perl:Depends}, ${misc:Depends}
Provides: ${cdbs:Provides}
Replaces: ${cdbs:Replaces}
Conflicts: ${cdbs:Conflicts}
Description: Perl module wrapper for libgd - gd2 variant
 This is a autoloadable interface module for libgd, a popular library
 for creating and manipulating PNG files.  With this library you can
 create PNG images on the fly or modify existing files.  Features
 include:
 .
  * Lines, polygons, rectangles and arcs, both filled and unfilled
  * Flood fills
  * The use of arbitrary images as brushes and as tiled fill patterns
  * Line styling (dashed lines and the like)
  * Horizontal and vertical text rendering
  * Support for transparency and interlacing
  * Support for TrueType font rendering, via libfreetype.
  * Support for spline curves, via GD::Polyline
  * Support for symbolic font names, such as "helvetica:italic"
  * Support for symbolic color names, such as "green", via GD::Simple
  * Produces output in png, gif, jpeg and xbm format
  * Produces output in svg format via GD::SVG.
 .
 Included with the example code is the perl script qd.pl with QuickDraw
 routines for generating PICT2 files (used on Apple Macintosh).
